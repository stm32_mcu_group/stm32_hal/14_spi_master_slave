/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
enum{
    TRANSFER_WAIT,
    TRANSFER_COMPLETE,
    TRANSFER_ERROR
};

#define SPI_ACK_BYTES       0xA5A5
#define SPI_NACK_BYTES      0xDEAD
#define SPI_TIMEOUT_MAX     0x1000
#define SPI_SLAVE_SYNBYTE   0x53
#define SPI_MASTER_SYNBYTE  0xAC


#define ADDR_CMD_MASTER_RD  ((uint16_t)0x1234)
#define ADDR_CMD_MASTER_WR  ((uint16_t)0x5678)
#define CMD_LEN             ((uint16_t)0x0004)
#define DAT_LEN             ((uint16_t)0x0020)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 SPI_HandleTypeDef hspi2;

/* USER CODE BEGIN PV */
uint8_t spi_rx_buffer[DAT_LEN] = {0};

uint8_t spi_tx_master_buffer[] = "SPI - MASTER - Transmit message";
uint8_t spi_tx_slave_buffer[] = "SPI - SLAVE - Transmit message";

volatile uint8_t transfer_state_flag = TRANSFER_WAIT;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
static uint16_t bufferCMP(uint8_t *pBuffer1, uint8_t* pBuffer2, uint16_t len){
    while(len--){
        if(*pBuffer1 != *pBuffer2)
            return len;
        pBuffer1++;
        pBuffer2++;
    }
    return 0;
}

static void slaveSync(){
    uint8_t tx_ack_byte = SPI_SLAVE_SYNBYTE;
    uint8_t rx_ack_byte = 0x00;

    do{
        if(HAL_SPI_TransmitReceive(&hspi2, &tx_ack_byte, &rx_ack_byte, 1, HAL_MAX_DELAY) != HAL_OK)
            Error_Handler();
    }while(rx_ack_byte != SPI_MASTER_SYNBYTE);
}

static void flushBuffer(uint8_t *pBuffer, uint16_t len){
    while(len--){
        *pBuffer = 0;
        pBuffer++;
    }
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi){
    HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
    transfer_state_flag = TRANSFER_COMPLETE;
}

void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi){
    transfer_state_flag = TRANSFER_ERROR;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint16_t addr_cmd = 0;
  uint16_t com_len = 0;
  uint8_t paddr_cmd[CMD_LEN] = {0};
  uint16_t ack_bytes = 0x0000;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI2_Init();
  /* USER CODE BEGIN 2 */

  /*if(HAL_SPI_TransmitReceive_IT(&hspi2, spi_tx_buffer, spi_rx_buffer, 8) != HAL_OK)
      Error_Handler();

  while(transfer_state_flag == TRANSFER_WAIT){}

    switch (transfer_state_flag) {
      case TRANSFER_COMPLETE:
          if(bufferCMP(spi_tx_buffer, spi_rx_buffer, 8))
              Error_Handler();
          break;
      default:
          Error_Handler();
          break;
    }*/

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
      // Handshake
      slaveSync();
      if(HAL_SPI_Receive_IT(&hspi2, paddr_cmd, CMD_LEN) != HAL_OK)
          Error_Handler();
      while(HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY){}

      addr_cmd = (uint16_t) ((paddr_cmd[0] << 8) | paddr_cmd[1]);
      com_len = (uint16_t)((paddr_cmd[2] <<8) | paddr_cmd[3]);
      // Read operation
      if(addr_cmd == ADDR_CMD_MASTER_RD || (addr_cmd == ADDR_CMD_MASTER_WR && com_len > 0)) {
          slaveSync();

          ack_bytes = SPI_ACK_BYTES;
          if (HAL_SPI_Transmit_IT(&hspi2, (uint8_t *) &ack_bytes, sizeof(ack_bytes)) != HAL_OK)
              Error_Handler();
          while (HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY) {}

          if (addr_cmd == ADDR_CMD_MASTER_RD) {
              slaveSync();
              if (HAL_SPI_Transmit_IT(&hspi2, spi_tx_slave_buffer, DAT_LEN) != HAL_OK)
                  Error_Handler();
              while (HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY) {}

              slaveSync();

              ack_bytes = 0;
              if (HAL_SPI_Receive_IT(&hspi2, (uint8_t *) &ack_bytes, sizeof(ack_bytes)) != HAL_OK)
                  Error_Handler();
              while (HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY) {}

              if (ack_bytes != SPI_ACK_BYTES)
                  Error_Handler();

          } else
              // Write operation
              if (addr_cmd == ADDR_CMD_MASTER_WR) {
              slaveSync();

              if(HAL_SPI_Receive_IT(&hspi2, spi_rx_buffer, DAT_LEN) != HAL_OK)
                  Error_Handler();
              while (HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY) {}

              slaveSync();

              ack_bytes = SPI_ACK_BYTES;
              if (HAL_SPI_Transmit_IT(&hspi2, (uint8_t *) &ack_bytes, sizeof(ack_bytes)) != HAL_OK)
                  Error_Handler();
              while (HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY) {}

              if(bufferCMP(spi_tx_master_buffer, spi_rx_buffer, DAT_LEN))
                  Error_Handler();
              else
                  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
          }
      }else{
          slaveSync();

          ack_bytes = SPI_NACK_BYTES;
          if (HAL_SPI_Transmit_IT(&hspi2, (uint8_t *) &ack_bytes, sizeof(ack_bytes)) != HAL_OK)
              Error_Handler();
          while (HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY) {}
          Error_Handler();
      }

      flushBuffer(spi_rx_buffer, DAT_LEN);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV6;
  RCC_OscInitStruct.PLL.PLLN = 85;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_SLAVE;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_HARD_INPUT;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while (1)
  {
      HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
      HAL_Delay(1000);
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

#pragma clang diagnostic pop
